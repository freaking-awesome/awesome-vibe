import vibe.vibe;



void index(HTTPServerRequest req, HTTPServerResponse res)
{
    res.render!("index.jade", req);
}



shared static this() {



    // Маршрутизатор

    auto router= new URLRouter;

    router.get("/", &index);



    // Сервер

    auto settings= new HTTPServerSettings;
    settings.port= 8080;

    listenHTTP(settings, router);

}



version (Test) {

    import specd.specd;
    import specd.reporter;

    int main() {
        auto reporter= new ConsoleReporter();
        if (reporter.report()) {

            return 0;

        } else {

            return 10; // Indicate failures so scripts can check result of running unit tests

        }
    }

    unittest {

        requestHTTP("http://localhost:8080", (scope req) {}, (scope res) {
                describe("a response")
                    .should("have a success status", res.statusCode.must.equal(200))
                ;
            }
        );
    }
}
